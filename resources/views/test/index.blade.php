<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>{{ $prueba->title }}</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/general.css') }}">
</head>
<body>
HOLA CÓDIGO FACILITO
<br><br>
<h1>{{ $prueba->title }}</h1>
<hr>
{{ $prueba->content }}
<hr>
{{ $prueba->user->name }} | {{ $prueba->category->name }} |

@foreach($prueba->tags as $tag)
    {{ $tag->name }} <!--Como tags con article es una relación Nx1 debe realizarse con un arreglo-->
@endforeach
</body>
</html>


