@extends('admin.template.main')

@section('title', 'Editar Usuario ' . $user->name)

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Editar usuario {{ $user->name }}</div>
        <div class="panel-body">
            {!! Form::open(['route' => ['admin.users.update', $user->id], 'method' => 'PUT']) !!}
            <div class="form-group">
                {!! Form::label('name', 'Nombre') !!}
                {!! Form::text('name', $user->name, ['class' => 'form-control', 'required', 'placeholder' => 'Nombre completo']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('email', 'Correo Electrónico') !!}
                {!! Form::email('email', $user->email, ['class' => 'form-control', 'required', 'placeholder' => 'example@gmail.com']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('type', 'Tipo') !!}
                {!! Form::select('type', ['member' => 'Miembro', 'admin' => 'Administrador'], $user->type, ['class' => 'form-control', 'placeholder' => 'Seleccione una opción...', 'required']) !!}
            </div>

            <div class="form-group">
                {!! Form::submit('Editar', ['class' => 'btn btn-primary']) !!}
            </div>

            {!! Form::close() !!}
        </div>
    </div>
@endsection
