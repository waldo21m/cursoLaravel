@extends('admin.template.main')

@section('title', 'Lista de usuarios')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Lista de usuarios</div>
        <div class="panel-body">
            <a href="{{ route('admin.users.create') }}" class="btn btn-info">Registrar nuevo usuario</a>
        </div>
        <!-- Table -->
        <table class="table">
            <thead>
            <th>ID</th>
            <th>Nombre</th>
            <th>Correo</th>
            <th>Tipo</th>
            <th>Acción</th>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{ $user->id }}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>
                        @if($user->type == "admin")
                            <span class="label label-danger">{{ $user->type }}</span>
                        @else
                            <span class="label label-primary">{{ $user->type }}</span>
                        @endif
                    </td>
                    <td>
                        <form method="POST" action="{{ route("admin.users.destroy", $user->id) }}">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <a href="{{ route('admin.users.edit', $user->id) }}" class="btn btn-warning"><span class="glyphicon glyphicon-wrench"></span></a>
                            <button type="submit" class="btn btn-danger" onclick="return confirm('¿Seguro que deseas eliminarlo?')"><span class="glyphicon glyphicon-remove-circle"></span></button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="text-center">
            {!! $users->render() !!}
        </div>
    </div>
@endsection
