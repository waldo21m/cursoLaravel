@extends('admin.template.main')

@section('title', 'Listado de imágenes')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Listado de imágenes</div>
        <div class="panel-body">
            <div class="row">
                @foreach($images as $image)
                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <img src="/images/articles/{{ $image->name }}" class="img-responsive">
                            </div>
                            <div class="panel-footer">{{ $image->article->title }}</div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="text-center">
            {!! $images->render() !!}
        </div>
    </div>
@endsection
