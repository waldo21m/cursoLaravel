<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <!--Con esto creamos el boton para dispositivos moviles que contienen
            todas las secciones del menú, cada span es cada línea del dibujo.-->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-1">
                <span class="sr-only">Menú</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!--Aquí va el logo o nombre de la empresa-->
            <a href="{{ url('/') }}" class="navbar-brand">Empresas Noffra</a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-1">
            @if(Auth::user())
                <ul class="nav navbar-nav">
                    <li><a href="{{ route('admin.index') }}">Inicio</a></li>
                    @if(Auth::user()->admin())
                        <li><a href="{{ route('admin.users.index') }}">Usuarios</a></li>
                    @endif
                    <li><a href="{{ route('admin.categories.index') }}">Categorías</a></li>
                    <li><a href="{{ route('admin.articles.index') }}">Artículos</a></li>
                    <li><a href="{{ route('admin.images.index') }}">Imágenes</a></li>
                    <li><a href="{{ route('admin.tags.index') }}">Tags</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="{{ route('front.index') }}" target="_blank">Página principal</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ route('admin.auth.logout') }}">Salir</a></li>
                        </ul>
                    </li>
                </ul>
            @else
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="{{ route('admin.auth.login') }}">Iniciar sesión</a></li>
                </ul>
            @endif
        </div>
    </div>
</nav>