@if(count($errors) > 0)
    <div class="alert alert-danger" role="alert">
        <h4>Por favor, corrige los siguientes errores:</h4>
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif