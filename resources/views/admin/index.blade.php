@extends('admin.template.main')

@section('title', 'Inicio') {{-- Esta es una manera --}}
{{-- Inicio de mi página --}} {{-- Manera más larga de realizarlo --}}
{{-- @endsection --}}

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Inicio</div>
        <div class="panel-body">
            <h1>Bienvenidos al panel de administración</h1>
            <hr>
            <a href="{{ route('admin.articles.create') }}" class="btn btn-link">Crear nuevo artículo</a> | <a href="{{ route('admin.tags.create') }}" class="btn btn-link">Crear nuevo tag</a>
        </div>
    </div>
@endsection
