@extends('admin.template.main')

@section('title', 'Agregar categoría')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Agregar categoría</div>
        <div class="panel-body">
            {!! Form::open(['route' => 'admin.categories.store', 'method' => 'POST']) !!}
            <div class="form-group">
                {!! Form::label('name', 'Nombre') !!}
                {!! Form::text('name', null, ['class' => 'form-control', 'required', 'placeholder' => 'Nombre de la categoría']) !!}
            </div>

            <div class="form-group">
                {!! Form::submit('Registrar', ['class' => 'btn btn-primary']) !!}
            </div>

            {!! Form::close() !!}
        </div>
    </div>
@endsection
