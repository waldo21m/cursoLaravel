@extends('admin.template.main')

@section('title', 'Editar categoría ' . $category->name)

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Editar categoría {{ $category->name }}</div>
        <div class="panel-body">
            {!! Form::open(['route' => ['admin.categories.update', $category->id], 'method' => 'PUT']) !!}
            <div class="form-group">
                {!! Form::label('name', 'Nombre') !!}
                {!! Form::text('name', $category->name, ['class' => 'form-control', 'required', 'placeholder' => 'Nombre de la categoría']) !!}
            </div>

            <div class="form-group">
                {!! Form::submit('Editar', ['class' => 'btn btn-primary']) !!}
            </div>

            {!! Form::close() !!}
        </div>
    </div>
@endsection
