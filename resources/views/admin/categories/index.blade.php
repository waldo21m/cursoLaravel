@extends('admin.template.main')

@section('title', 'Listado de categorías')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Listado de categorías</div>
        <div class="panel-body">
            <a href="{{ route('admin.categories.create') }}" class="btn btn-info">Registrar nueva categoría</a>
        </div>
        <!-- Table -->
        <table class="table">
            <thead>
            <th>ID</th>
            <th>Nombre</th>
            <th>Acción</th>
            </thead>
            <tbody>
            @foreach($categories as $category)
                <tr>
                    <td>{{ $category->id }}</td>
                    <td>{{ $category->name }}</td>
                    <td>
                        <form method="POST" action="{{ route("admin.categories.destroy", $category->id) }}">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <a href="{{ route('admin.categories.edit', $category->id) }}" class="btn btn-warning"><span class="glyphicon glyphicon-wrench"></span></a>
                            <button type="submit" class="btn btn-danger" onclick="return confirm('¿Seguro que deseas eliminarlo?')"><span class="glyphicon glyphicon-remove-circle"></span></button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="text-center">
            {!! $categories->render() !!}
        </div>
    </div>
@endsection
