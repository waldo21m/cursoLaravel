@extends('admin.template.main')

@section('title', 'Agregar artículo')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Agregar artículo</div>
        <div class="panel-body">
            {!! Form::open(['route' => 'admin.articles.store', 'method' => 'POST', 'files' => true]) !!}
            <div class="form-group">
                {!! Form::label('title', 'Título') !!}
                {!! Form::text('title', null, ['class' => 'form-control', 'required', 'placeholder' => 'Título del artículo']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('category_id', 'Categoría') !!}
                {!! Form::select('category_id', $categories, null, ['class' => 'form-control select-category', 'required']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('content', 'Contenido') !!}
                {!! Form::textarea('content', null, ['class' => 'form-control textarea-content']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('tags', 'Tags') !!}
                {!! Form::select('tags[]', $tags, null, ['class' => 'form-control select-tag', 'multiple']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('image', 'Imágen') !!}
                {!! Form::file('image') !!}
            </div>

            <div class="form-group">
                {!! Form::submit('Agregar artículo', ['class' => 'btn btn-primary']) !!}
            </div>

            {!! Form::close() !!}
        </div>
    </div>
@endsection
@section('js')
    <script>
        $('.select-tag').chosen({
            placeholder_text_multiple: 'Seleccione un máximo de 3 tags',
            max_selected_options: 3,
            no_results_text: 'No se encontró este tag'
        });

        $('.select-category').chosen({
            placeholder_text_single: 'Seleccione una categoría',
            no_results_text: 'No se encontró esta categoría'
        });

        $('.textarea-content').trumbowyg();
    </script>
@endsection
