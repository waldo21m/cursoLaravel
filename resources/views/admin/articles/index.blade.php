@extends('admin.template.main')

@section('title', 'Listado de artículos')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Listado de artículos</div>
        <div class="panel-body">
            <a href="{{ route('admin.articles.create') }}" class="btn btn-info">Registrar nuevo artículo</a>
            {{--BUSCADOR DE ARTÍCULOS--}}
            {!! Form::open(['route' => 'admin.articles.index', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
            <div class="input-group">
                {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Buscar artículo...', 'aria-describedby' => 'search']) !!}
                <span class="input-group-btn">
                    <button class="btn btn-primary" type="submit" id="search">
                        <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                    </button>
                </span>
            </div>
            {!! Form::close() !!}
            {{--FIN DEL BUSCADOR--}}
        </div>
        <!-- Table -->
        <table class="table">
            <thead>
            <th>ID</th>
            <th>Título</th>
            <th>Categoría</th>
            <th>Usuario</th>
            <th>Acción</th>
            </thead>
            <tbody>
            @foreach($articles as $article)
                <tr>
                    <td>{{ $article->id }}</td>
                    <td>{{ $article->title }}</td>
                    <td>{{ $article->category->name }}</td>
                    <td>{{ $article->user->name }}</td>
                    <td>
                        <form method="POST" action="{{ route("admin.articles.destroy", $article->id) }}">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <a href="{{ route('admin.articles.edit', $article->id) }}" class="btn btn-warning"><span class="glyphicon glyphicon-wrench"></span></a>
                            <button type="submit" class="btn btn-danger" onclick="return confirm('¿Seguro que deseas eliminarlo?')"><span class="glyphicon glyphicon-remove-circle"></span></button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="text-center">
            {!! $articles->render() !!}
        </div>
    </div>
@endsection

