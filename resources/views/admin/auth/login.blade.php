@extends('admin.template.main')

@section('title', 'Login')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Login</div>
        <div class="panel-body">
            {!! Form::open(['route' => 'admin.auth.login', 'method' => 'POST']) !!}
            <div class="form-group">
                {!! Form::label('email', 'Correo electrónico') !!}
                {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'example@gmail.com']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('password', 'Contraseña') !!}
                {!! Form::password('password', ['class' => 'form-control', 'placeholder' => '***********']) !!}
            </div>

            <div class="form-group">
                {!! Form::submit('Acceder', ['class' => 'btn btn-primary']) !!}
            </div>

            {!! Form::close() !!}
        </div>
    </div>
@endsection
