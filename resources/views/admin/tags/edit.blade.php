@extends('admin.template.main')

@section('title', 'Editar tag ' . $tag->name)

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Editar tag {{ $tag->name }}</div>
        <div class="panel-body">
            {!! Form::open(['route' => ['admin.tags.update', $tag->id], 'method' => 'PUT']) !!}
            <div class="form-group">
                {!! Form::label('name', 'Nombre') !!}
                {!! Form::text('name', $tag->name, ['class' => 'form-control', 'required', 'placeholder' => 'Nombre del tag']) !!}
            </div>

            <div class="form-group">
                {!! Form::submit('Editar', ['class' => 'btn btn-primary']) !!}
            </div>

            {!! Form::close() !!}
        </div>
    </div>
@endsection
