@extends('admin.template.main')

@section('title', 'Agregar tag')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Agregar tag</div>
        <div class="panel-body">
            {!! Form::open(['route' => 'admin.tags.store', 'method' => 'POST']) !!}
            <div class="form-group">
                {!! Form::label('name', 'Nombre') !!}
                {!! Form::text('name', null, ['class' => 'form-control', 'required', 'placeholder' => 'Nombre del tag']) !!}
            </div>

            <div class="form-group">
                {!! Form::submit('Registrar', ['class' => 'btn btn-primary']) !!}
            </div>

            {!! Form::close() !!}
        </div>
    </div>
@endsection
