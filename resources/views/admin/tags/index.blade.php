@extends('admin.template.main')

@section('title', 'Listado de tags')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Listado de tags</div>
        <div class="panel-body">
            <a href="{{ route('admin.tags.create') }}" class="btn btn-info">Registrar nuevo tag</a>
            {{--BUSCADOR DE TAGS--}}
            {!! Form::open(['route' => 'admin.tags.index', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
            <div class="input-group">
                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Buscar tag...', 'aria-describedby' => 'search']) !!}
                <span class="input-group-btn">
                    <button class="btn btn-primary" type="submit" id="search">
                        <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                    </button>
                </span>
            </div>
            {!! Form::close() !!}
            {{--FIN DEL BUSCADOR--}}
        </div>
        <!-- Table -->
        <table class="table">
            <thead>
            <th>ID</th>
            <th>Nombre</th>
            <th>Acción</th>
            </thead>
            <tbody>
            @foreach($tags as $tag)
                <tr>
                    <td>{{ $tag->id }}</td>
                    <td>{{ $tag->name }}</td>
                    <td>
                        <form method="POST" action="{{ route("admin.tags.destroy", $tag->id) }}">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <a href="{{ route('admin.tags.edit', $tag->id) }}" class="btn btn-warning"><span
                                        class="glyphicon glyphicon-wrench"></span></a>
                            <button type="submit" class="btn btn-danger"
                                    onclick="return confirm('¿Seguro que deseas eliminarlo?')"><span
                                        class="glyphicon glyphicon-remove-circle"></span></button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="text-center">
            {!! $tags->render() !!}
        </div>
    </div>
@endsection
