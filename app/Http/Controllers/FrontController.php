<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Article;
use App\Category;
use App\Tag;
use Carbon\Carbon;

class FrontController extends Controller
{
    public function __construct()
    {
        Carbon::setLocale('es');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$articles = Article::orderBy('id', 'DESC')->get()->take(2);
        $articles = Article::orderBy('id', 'DESC')->paginate(2);
        $articles->each(function ($articles) {
            $articles->category;
            $articles->images;
        });

        return view('front.index', ['articles' => $articles]);
    }

    public function searchCategory($name)
    {
        $category = Category::SearchCategory($name)->first();
        $articles = $category->articles()->paginate(2);
        $articles->each(function ($articles) {
            $articles->category;
            $articles->images;
        });

        return view('front.index', ['articles' => $articles]);
    }

    public function searchTag($name)
    {
        $tag = Tag::SearchTag($name)->first();
        $articles = $tag->articles()->paginate(2);
        $articles->each(function ($articles) {
            $articles->category;
            $articles->images;
        });

        return view('front.index', ['articles' => $articles]);
    }

    public function viewArticle($slug)
    {
        $article = Article::findBySlugOrFail($slug);
        $article->category;
        $article->user;
        $article->tags;
        $article->images;

        return view('front.article', ['article' => $article]);
    }
}
