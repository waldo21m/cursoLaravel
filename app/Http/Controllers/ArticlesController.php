<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Category;
use App\Tag;
use App\Article;
use Illuminate\Support\Facades\Auth;
use App\Image;
use Laracasts\Flash\Flash;
use App\Http\Requests\ArticleRequest;

class ArticlesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $articles = Article::search($request->title)->orderBy('id', 'DESC')->paginate(5);
        //Al paginar, se debe usar este método para acceder a las relaciones
        $articles->each(function ($articles) {
            $articles->category;
            $articles->user;
        });

        return view('admin.articles.index', ['articles' => $articles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Esto sirve para los selectbox con laravelcollective
        $categories = Category::orderBy('name', 'ASC')->lists('name', 'id');
        //dd($categories);

        //En caso de no usar laravel collective, se debe hacer el recorrido de la siguiente manera
        //foreach ($categories as $field => $value) {
        //    echo $field . " y el nombre es " . $value . "<br>";
        //}
        //exit;
        $tags = Tag::orderBy('name', 'ASC')->lists('name', 'id');

        return view('admin.articles.create', ['categories' => $categories, 'tags' => $tags]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleRequest $request)
    {
        //Manipulación de imágenes
        if ($request->file('image')) {
            $file = $request->file('image');
            $name = 'blogfacilito_' . time() . '.' . $file->getClientOriginalExtension();
            $path = public_path() . '/images/articles/';
            $file->move($path, $name);
        }

        $article = new Article($request->all());
        $article->user_id = Auth::user()->id;
        $article->save();

        if (isset($request->tags)) {
            $article->tags()->sync($request->tags);
        }

        $image = new Image();
        $image->name = $name;
        //Esto es válido también
        //$image->article_id = $article->id;
        $image->article()->associate($article);
        $image->save();

        Flash::success('Se ha creado el artículo ' . $article->title . ' de forma satisfactoria!');

        return redirect()->route('admin.articles.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = Article::find($id);
        $article->category;
        $categories = Category::orderBy('name', 'DESC')->lists('name', 'id');
        $tags = Tag::orderBy('name', 'ASC')->lists('name', 'id');

        /*
         * La etiqueta 'multiple' de html collective trabaja solamente con un array. Para ello,
         * buscamos todos los tags, los listamos y lo transformamos a un array con el método
         * proporcionado por laravel toArray()
         */
        $myTags = $article->tags->lists('id')->toArray();

        return view('admin.articles.edit', ['article' => $article, 'categories' => $categories, 'tags' => $tags, 'myTags' => $myTags]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $article = Article::find($id);
        $article->fill($request->all());
        $article->save();

        if (isset($request->tags)) {
            $article->tags()->sync($request->tags);
        }
        Flash::warning('Se ha editado el artículo ' . $article->title . ' de forma exitosa!');

        return redirect()->route('admin.articles.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $article = Article::find($id);
        $article->delete();

        Flash::error('Se ha borrado el artículo ' . $article->title . ' de forma exitosa!');
        return redirect()->route('admin.articles.index');
    }
}
