<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
 * Las rutas existentes son: GET, POST, PUT, DELETE
 */

//RUTAS DEL FRONTEND
Route::get('/', [
    'uses' => 'FrontController@index',
    'as' => 'front.index',
]);

Route::get('categories/{name}', [
    'uses' => 'FrontController@searchCategory',
    'as' => 'front.search.category',
]);

Route::get('tags/{name}', [
    'uses' => 'FrontController@searchTag',
    'as' => 'front.search.tag',
]);

Route::get('articles/{slug}', [
    'uses' => 'FrontController@viewArticle',
    'as' => 'front.view.article',
]);

#Ruta normal
/*Route::get('articles', function () {
   echo "Esta es la sección de artículos";
});*/

#Ruta con paso de parámetros
/*Route::get('articles/{nombre}', function ($nombre) {
    echo "El nombre que has colocado es: " . $nombre;
});*/

#Ruta con parámetro opcional
/*Route::get('articles/{nombre?}', function ($nombre = "No coloco nombre") {
    echo "El nombre que has colocado es: " . $nombre;
});*/

#Grupo de rutas con prefijos
/*Route::group(['prefix' => 'articles'], function () {
    Route::get('view/{article?}', function ($article = "Vacío") {
        echo $article;
    });
    Route::get('view/{id}', [
        'uses'  =>  'TestController@view',
        'as'    =>  'articlesView'
    ]);
});*/

//RUTAS DEL PANEL DE ADMINISTRACIÓN
Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
    /*
     * Con el método resource crea múltiples rutas para manejar una variedad de
     * acciones RESTful en el recurso del usuario. Del mismo modo, el controlador
     * generado ya tendrá métodos para cada una de estas acciones, incluidas notas
     * que le informan qué URI y verbos manejan.
     * https://laravel.com/docs/5.1/controllers#restful-resource-controllers
     */
    Route::get('/', [
        'as' => 'admin.index',
        function () {
            return view('admin.index');
        },
    ]);

    Route::group(['middleware' => 'admin'], function () {
        Route::resource('users', 'UsersController');
    });

    Route::resource('categories', 'CategoriesController');

    Route::resource('tags', 'TagsController');

    Route::resource('articles', 'ArticlesController');

    Route::get('images', [
        'uses' => 'ImagesController@index',
        'as' => 'admin.images.index',
    ]);
});

Route::group(['prefix' => 'admin/auth'], function () {
    Route::get('login', [
        'uses' => 'Auth\AuthController@getLogin',
        'as' => 'admin.auth.login',
    ]);
    Route::post('login', [
        'uses' => 'Auth\AuthController@postLogin',
        'as' => 'admin.auth.login',
    ]);
    Route::get('logout', [
        'uses' => 'Auth\AuthController@getLogout',
        'as' => 'admin.auth.logout',
    ]);
});