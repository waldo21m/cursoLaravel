<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //Por defecto, viene desactivado. Debemos activarlo si queremos usar request
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'min:4|max:120|required',
            'email' => 'between:4,250|required|unique:users,email|email',
            'password' => 'between:4,120|required',
        ];
    }
}
