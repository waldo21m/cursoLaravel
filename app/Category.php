<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    #El primer atributo lleva el nombre de la tabla
    protected $table = "categories";

    #El atributo fillable son los campos permitidos para mostrar los objetos JSON, es decir,
    #con esto se mostrarán los campos que deseas de la BDD, en este ejemplo, solo name
    protected $fillable = ['name'];

    #Relación 1xN entre Categorias con Articulos
    #Cuando es plural significa que es muchos
    public function articles()
    {
        return $this->hasMany('App\Article');
    }

    public function scopeSearchCategory($query, $name)
    {
        return $query->where('name', '=', $name);
    }
}
